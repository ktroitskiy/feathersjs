// Initializes the `test-service` service on path `/test-service`
const createService = require('feathers-nedb');
const createModel = require('../../models/test-service.model');
const hooks = require('./test-service.hooks');
const filters = require('./test-service.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'test-service',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/test-service', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('test-service');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
