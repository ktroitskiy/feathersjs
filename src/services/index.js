const testService = require('./test-service/test-service.service.js');
module.exports = function () {
  const app = this; // eslint-disable-line no-unused-vars
  app.configure(testService);
};
